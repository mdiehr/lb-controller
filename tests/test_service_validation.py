import pytest
import kubernetes
from lb_controller import k8s_cache
import pprint

@pytest.fixture(scope="function")
def cache():
    return k8s_cache.K8sCache('192.168.1.0/24')

@pytest.fixture(scope="function")
def cache6():
    return k8s_cache.K8sCache('2001:db8::1000/124')

@pytest.fixture(scope="function")
def cache46():
    return k8s_cache.K8sCache('192.168.1.0/24', '2001:db8::1000/124')

# Helpers
def get_base_svc(namespace, name, svc_type, annotations={}):
    svc = kubernetes.client.V1Service()
    svc.metadata = kubernetes.client.V1ObjectMeta(name=name, namespace=namespace, annotations=annotations, resource_version='32')
    svc.spec = kubernetes.client.V1ServiceSpec(type=svc_type)
    return svc

def get_valid_svc(namespace='default', name='toto', svc_type='LoadBalancer', exposed_ip='192.168.1.1', node_port=30001, target_port=8080, svc_port=666, annotations={}):
    svc = get_base_svc(namespace, name, svc_type, annotations)
    svc.spec.load_balancer_ip = exposed_ip
    port = kubernetes.client.V1ServicePort(node_port=node_port, name='stuff', target_port=target_port, protocol='TCP', port=svc_port)
    svc.spec.ports = [port]
    return svc

# Real tests
def test_valid_service_lb(cache):
    svc = get_valid_svc()
    valid, cause = cache.is_valid_service(svc)
    assert valid

def test_valid_service_nodeport(cache):
    svc = get_valid_svc(svc_type='NodePort', annotations={'external-lb/ip-address':'192.168.1.1'})
    valid, cause = cache.is_valid_service(svc)
    assert valid

def test_invalid_service_type(cache):
    svc = get_valid_svc(svc_type='ClusterIP')
    assert not cache.is_exposed_service(svc)

def test_invalid_unannotated_nodeport(cache):
    svc = get_valid_svc(svc_type='NodePort')
    assert not cache.is_exposed_service(svc)

def test_exposed_service_nodeport(cache):
    svc = get_valid_svc(svc_type='NodePort', annotations={'external-lb/ip-address':'192.168.1.1'})
    assert cache.is_exposed_service(svc)

def test_valid_svc_multiple_ports(cache):
    svc = get_valid_svc()
    svc.spec.ports.append(kubernetes.client.V1ServicePort(node_port=30002, name='other_stuff', target_port=8443, protocol='TCP', port=8443))
    assert cache.is_valid_service(svc)
    valid, cause = cache.is_valid_service(svc)
    assert valid

def test_change_first_insertion(cache):
    svc = get_valid_svc()
    assert cache.upsert_service(svc)

def test_no_change_same_insertion(cache):
    svc = get_valid_svc()
    assert cache.upsert_service(svc)
    assert not cache.upsert_service(svc)

def test_change_new_revision(cache):
    svc1 = get_valid_svc()
    svc2 = get_valid_svc()
    svc2.metadata.resource_version = '33'
    assert cache.upsert_service(svc1)
    assert cache.upsert_service(svc2)

def test_conflict(cache):
    svc1 = get_valid_svc()
    svc2 = get_valid_svc(name='tutu')
    assert cache.upsert_service(svc1)
    valid, cause = cache.is_valid_service(svc2)
    assert not valid
    assert 'conflict' in cause

def test_no_conflict_after_deletion(cache):
    svc1 = get_valid_svc()
    svc2 = get_valid_svc(name='tutu')
    assert cache.upsert_service(svc1)
    assert cache.delete_service(svc1)
    valid, cause = cache.is_valid_service(svc2)
    assert valid

def test_existence(cache):
    svc = get_valid_svc()
    assert not cache.exists_service(svc)
    assert cache.upsert_service(svc)
    assert cache.exists_service(svc)

def test_invalid_range_ip(cache):
    svc = get_valid_svc(exposed_ip='192.168.0.1')
    valid, cause = cache.is_valid_service(svc)
    assert not valid
    assert 'not in a valid range' in cause

def test_svc_is_valid_if_ipv4_is_in_range(cache):
    svc = get_valid_svc(exposed_ip='192.168.1.1')
    valid, cause = cache.is_valid_service(svc)
    assert valid

def test_svc_is_valid_if_ipv6_is_in_range(cache6):
    svc = get_valid_svc(exposed_ip='2001:db8::1001')
    valid, cause = cache6.is_valid_service(svc)
    assert valid

def test_svc_is_invalid_if_ipv6_is_not_in_range(cache6):
    svc = get_valid_svc(exposed_ip='2001:db9::1001')
    valid, cause = cache6.is_valid_service(svc)
    assert not valid

def test_svc_is_invalid_if_ipv6_and_cache_range_in_v4(cache):
    svc = get_valid_svc(exposed_ip='2001:db9::1001')
    valid, cause = cache.is_valid_service(svc)
    assert not valid

def test_svc_is_invalid_if_ipv4_and_cache_range_in_v6(cache6):
    svc = get_valid_svc(exposed_ip='192.168.1.1')
    valid, cause = cache6.is_valid_service(svc)
    assert not valid

def test_svcs_are_valid_if_cache_with_v4_and_v6_ranges(cache46):
    svc4 = get_valid_svc(exposed_ip='192.168.1.1')
    svc6 = get_valid_svc(exposed_ip='2001:db8::1001')

    valid4, cause = cache46.is_valid_service(svc4)
    valid6, cause = cache46.is_valid_service(svc6)

    assert valid4
    assert valid6

def test_svc_is_valid_if_ipv4_contains_network_mask(cache):
    svc = get_valid_svc(exposed_ip='192.168.1.1/24')
    valid, cause = cache.is_valid_service(svc)
    assert valid

def test_svc_is_invalid_if_ipv4_not_in_range_and_contains_network_mask(cache):
    svc = get_valid_svc(exposed_ip='192.168.2.1/24')
    valid, cause = cache.is_valid_service(svc)
    assert not valid
    assert 'not in a valid range' in cause

def test_svc_is_valid_if_ipv6_contains_network_mask(cache6):
    svc = get_valid_svc(exposed_ip='2001:db8::1001/124')
    valid, cause = cache6.is_valid_service(svc)
    assert valid

def test_svc_is_invalid_if_ipv6_not_in_range_and_contains_network_mask(cache6):
    svc = get_valid_svc(exposed_ip='2001:db9::1001/124')
    valid, cause = cache6.is_valid_service(svc)
    assert not valid

def test_svc_is_invalid_if_ip_is_gibberish(cache):
    svc = get_valid_svc(exposed_ip='this_is_not_a_valid_ip')
    valid, cause = cache.is_valid_service(svc)
    assert not valid

def test_svc_is_invalid_if_ip_is_gibberish_nodeport(cache):
    svc = get_valid_svc(svc_type='NodePort', annotations={'external-lb/ip-address':'sdfkljsdklfjs'})
    valid, cause = cache.is_valid_service(svc)
    assert not valid
