import pytest
import asyncio
import logging
import kubernetes_asyncio as kubernetes
from lb_controller import event_handler, k8s_cache

def generate_fake_svc(name='nginx', ns='default', type='LoadBalancer', lb_ip='192.168.1.1'):
    return kubernetes.client.V1Service(metadata=kubernetes.client.V1ObjectMeta(name=name, namespace=ns),
                                       spec=kubernetes.client.V1ServiceSpec(type=type, load_balancer_ip=lb_ip))

def append_event(action, payload):
    ev = {'type': action, 'object': payload}
    return ev

def generate_svc_creation_event(name='nginx', ns='default', type='LoadBalancer', lb_ip='192.168.1.1'):
    svc = generate_fake_svc(name=name, ns=ns, type=type, lb_ip=lb_ip)
    return append_event('ADDED', svc)

class V1APIMock:
    def __init__(self):
        self._svc_items = []
        self._node_items = []
        self._patches = []
        self._status_patches = []

    @property
    def patches(self):
        return self._patches

    @property
    def status_patches(self):
        return self._status_patches

    async def patch_namespaced_service(self, name, namespace, patch):
        self._patches.append((name, namespace, patch))
        return generate_fake_svc(name=name, ns=namespace)

    async def replace_namespaced_service_status(self, name, namespace, new_status):
        self._status_patches.append((name, namespace, new_status))
        return generate_fake_svc(name=name, ns=namespace)

class DaemonControllerMock:
    def __init__(self):
        self._calls_count = 0
        self._kg_calls_count = 0

    @property
    def keep_going(self):
        return self._calls_count == 0

    def sync_config(self):
        self._calls_count += 1

    @property
    def calls_count(self):
        return self._calls_count

@pytest.fixture(scope="function")
def v1_api():
    return V1APIMock()

@pytest.fixture(scope="function")
def daemon_controller():
    return DaemonControllerMock()

@pytest.fixture(scope="function")
def cache():
    return k8s_cache.K8sCache('192.168.1.0/24')

def test_one_normal_object_creation(v1_api, cache, daemon_controller):
    event = generate_svc_creation_event(lb_ip='192.168.1.1')
    loop = asyncio.get_event_loop()
    task1 = loop.create_task(event_handler.regenerate_config(daemon_controller, 0.1))
    task2 = loop.create_task(event_handler.handle_service_event(v1_api, cache, event))
    loop.run_until_complete(asyncio.gather(task1, task2))
    assert len(v1_api.patches) == 1
    assert len(v1_api.status_patches) == 1
    assert daemon_controller.calls_count == 1
    name, namespace, patch = v1_api.patches[0]
    assert name == 'nginx'
    assert namespace == 'default'
    assert patch['metadata']['annotations']['external-lb/sync-status'] == 'valid'
    assert patch['metadata']['annotations']['external-lb/sync-status-cause'] is None
    name, namespace, status_patch = v1_api.status_patches[0]
    assert status_patch.status.load_balancer.ingress[0].ip == '192.168.1.1'

def test_one_abnormal_object_creation_no_ip(v1_api, cache, daemon_controller):
    event = generate_svc_creation_event(lb_ip=None)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(event_handler.handle_service_event(v1_api, cache, event))
    assert len(v1_api.patches) == 1
    assert len(v1_api.status_patches) == 1
    name, namespace, patch = v1_api.patches[0]
    assert name == 'nginx'
    assert namespace == 'default'
    assert patch['metadata']['annotations']['external-lb/sync-status'] == 'invalid'
    assert 'no IP' in patch['metadata']['annotations']['external-lb/sync-status-cause']
    name, namespace, status_patch = v1_api.status_patches[0]
    assert status_patch.status.load_balancer == None

def test_one_abnormal_object_creation_not_loadbalancer(v1_api, cache, daemon_controller):
    event = generate_svc_creation_event(type='ClusterIP')
    loop = asyncio.get_event_loop()
    loop.run_until_complete(event_handler.handle_service_event(v1_api, cache, event))
    assert len(v1_api.patches) == 0
    assert len(v1_api.status_patches) == 0
