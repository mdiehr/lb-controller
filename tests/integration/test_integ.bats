#!/usr/bin/env bats
# vim:syntax=bash

create_lb_svc() {
  kubectl create -f - << __EOF__
apiVersion: v1
kind: Service
metadata:
  name: $1
  labels:
    test: test-bats
    testid: 'test-$BATS_TEST_NUMBER'
spec:
  loadBalancerIP: $2
  ports:
  - nodePort: $3
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    run: nginx
  type: LoadBalancer
__EOF__
}

create_np_svc() {
  kubectl create -f - << __EOF__
apiVersion: v1
kind: Service
metadata:
  labels:
    test: test-bats
    testid: 'test-$BATS_TEST_NUMBER'
  name: $1
  annotations: {$2}
spec:
  ports:
  - nodePort: $3
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    run: nginx
  type: NodePort
__EOF__
}

wait_for() {
  # $1: loop count
  # $2: delay between attempts
  # $3: expected return code
  # $@ command to run
  lopps=$1
  delay=$2
  exp_code=$3
  shift 3
  i=0
  for i in $(seq 1 $loop); do
    run "$@"
    if [ "$status" -eq "$exp_code" ]; then
      return 0
    fi
    sleep $delay
  done
  return 1
}

setup() {
  run kubectl delete svc -l "test=test-bats"
}

teardown() {
  run kubectl delete svc -l "test=test-bats"
}

@test "Test-$BATS_TEST_NUMBER create lb svc ok" {
  create_lb_svc test-svc 172.17.255.254 0
  wait_for 30 0.1 0 bash -c "kubectl get svc test-svc -o yaml | fgrep -q 'external-lb/sync-status: valid'"
  kubectl get svc test-svc --no-headers | fgrep -q '172.17.255.254'
}

@test "Test-$BATS_TEST_NUMBER create lb svc invalid ip" {
  create_lb_svc test-svc 172.17.255.254.566 0
  wait_for 30 0.1 0 bash -c "kubectl get svc test-svc -o yaml | fgrep -q 'external-lb/sync-status: invalid'"
  kubectl get svc test-svc -o yaml | fgrep -q 'not a valid Address'
}

@test "Test-$BATS_TEST_NUMBER create np svc invalid ip" {
  create_np_svc test-svc "external-lb/ip-address: 172.17.255.254.EEEE" 0
  wait_for 30 0.1 0 bash -c "kubectl get svc test-svc -o yaml | fgrep -q 'external-lb/sync-status: invalid'"
  kubectl get svc test-svc -o yaml | fgrep -q 'not a valid Address'
}

@test "Test-$BATS_TEST_NUMBER: create np svc ok" {
  create_np_svc test-svc "external-lb/ip-address: 172.17.255.254" 0
  wait_for 30 0.1 0 bash -c "kubectl get svc test-svc -o yaml | fgrep -q 'external-lb/sync-status: valid'"
  kubectl get svc test-svc --no-headers | fgrep -q '<none>'
}

@test "Test-$BATS_TEST_NUMBER: create lb svc conflict ko" {
  create_lb_svc test-svc 172.17.255.254 0
  wait_for 30 0.1 0 bash -c "kubectl get svc test-svc -o yaml | fgrep -q 'external-lb/sync-status: valid'"
  create_lb_svc test-svc2 172.17.255.254 0
  wait_for 30 0.1 0 bash -c "kubectl get svc test-svc2 -o yaml | fgrep -q 'external-lb/sync-status: invalid'"
  kubectl get svc test-svc2 -o yaml | fgrep -q 'IP conflicts: 172.17.255.254 already owned'
  kubectl get svc test-svc --no-headers | fgrep -q '172.17.255.254'
  kubectl get svc test-svc2 --no-headers | fgrep -q '<pending>'
}

@test "Test-$BATS_TEST_NUMBER: create np svc conflict ko" {
  create_np_svc test-svc "external-lb/ip-address: 172.17.255.254" 0
  wait_for 30 0.1 0 bash -c "kubectl get svc test-svc -o yaml | fgrep -q 'external-lb/sync-status: valid'"
  create_np_svc test-svc2 "external-lb/ip-address: 172.17.255.254" 0
  wait_for 30 0.1 0 bash -c "kubectl get svc test-svc2 -o yaml | fgrep -q 'external-lb/sync-status: invalid'"
  kubectl get svc test-svc2 -o yaml | fgrep -q 'IP conflicts: 172.17.255.254 already owned'
}
