import pytest

from lb_controller import k8s_cache

def test_k8s_cache_can_be_instanciated_with_v4_range():
    assert k8s_cache.K8sCache('192.168.1.0/24')

def test_k8s_cache_can_be_instanciated_with_2_v4_ranges():
    assert k8s_cache.K8sCache('192.168.1.0/24', '192.168.2.0/24')

def test_k8s_cache_cannot_be_instanciated_with_2_overlaping_v4_ranges():
    with pytest.raises(AssertionError) as e:
        k8s_cache.K8sCache('192.168.1.0/24', '192.168.1.128/28')
    assert e

def test_k8s_cache_can_be_instanciated_with_v6_address():
    assert k8s_cache.K8sCache('2001:db8::1000/124')

def test_k8s_cache_can_be_instanciated_with_2_v6_ranges():
    assert k8s_cache.K8sCache('2001:db8::1000/124', '2001:db8::1010/124')

def test_k8s_cache_cannot_be_instanciated_with_2_overlaping_v6_ranges():
    with pytest.raises(AssertionError) as e:
        k8s_cache.K8sCache('2001:db8::1000/124', '2001:db8::1008/125')
    assert e

def test_k8s_cache_can_be_instanciated_with_v4_and_v6_ranges():
    assert k8s_cache.K8sCache('2001:db8::1000/124', '192.168.1.0/24')
